package au.thoughtworks.mypokemon.ui.details;

import android.content.Intent;
import au.thoughtworks.mypokemon.model.Pokemon;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static au.thoughtworks.mypokemon.ui.details.DetailsActivity.EXTRA_POKEMON_DETAILS;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DetailsPresenterTest {
    @Mock
    Intent intent;

    @Mock
    DetailsView detailsView;

    DetailsPresenter detailsPresenter;

    @Before
    public void setUp() throws Exception {
        detailsPresenter = new DetailsPresenter();
        detailsPresenter.attachView(detailsView);
    }

    @Test
    public void shoudShowPokemonDetailOnCreate() throws Exception {
        String name = "Pikachu";
        String type = "Electric";
        int attack = 5;
        int defence = 10;
        String image = "someUrl";

        when(intent.getSerializableExtra(EXTRA_POKEMON_DETAILS))
                .thenReturn(new Pokemon(name, attack, defence, type, image));

        detailsPresenter.onCreate(intent);

        verify(detailsView).showPokemonDetails(name, attack, defence, type, image);
    }
}