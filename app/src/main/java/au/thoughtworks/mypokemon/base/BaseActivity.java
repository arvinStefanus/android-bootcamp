package au.thoughtworks.mypokemon.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import au.thoughtworks.mypokemon.base.injection.ActivityComponent;
import au.thoughtworks.mypokemon.base.injection.ActivityModule;

import javax.inject.Inject;

public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity {

    @Inject
    protected P presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        ActivityComponent activityComponent = ((MyPokemonApplication) getApplication()).getApplicationComponent()
                .newActivityComponent(new ActivityModule());
        inject(activityComponent);
    }

    protected abstract void inject(ActivityComponent activityComponent);
}
