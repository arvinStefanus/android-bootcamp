package au.thoughtworks.mypokemon.base.injection;

import au.thoughtworks.mypokemon.ui.details.DetailsActivity;
import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(DetailsActivity detailsActivity);
}
