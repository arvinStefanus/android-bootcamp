package au.thoughtworks.mypokemon.base.injection;

import dagger.Component;

@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    ActivityComponent newActivityComponent(ActivityModule activityModule);
}
