package au.thoughtworks.mypokemon.base;

import android.app.Application;
import au.thoughtworks.mypokemon.base.injection.ApplicationComponent;
import au.thoughtworks.mypokemon.base.injection.ApplicationModule;
import au.thoughtworks.mypokemon.base.injection.DaggerApplicationComponent;

public class MyPokemonApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
