package au.thoughtworks.mypokemon.base;

import java.lang.ref.WeakReference;

public abstract class BasePresenter<V extends BaseView> {
    private WeakReference<V> view;

    public void attachView(V view) {
        this.view = new WeakReference<>(view);
    }

    public V getView() {
        return view.get();
    }
}
