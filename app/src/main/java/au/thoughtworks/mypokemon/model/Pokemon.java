package au.thoughtworks.mypokemon.model;

import java.io.Serializable;

public class Pokemon implements Serializable {
    private String name;
    private String type;
    private int attack;
    private int defence;
    private String image;

    public Pokemon(String name, int attack, int defence, String type, String image) {
        this.name = name;
        this.type = type;
        this.attack = attack;
        this.defence = defence;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefence() {
        return defence;
    }

    public String getImage() {
        return image;
    }
}
