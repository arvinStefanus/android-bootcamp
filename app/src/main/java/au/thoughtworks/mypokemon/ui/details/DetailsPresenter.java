package au.thoughtworks.mypokemon.ui.details;

import android.content.Intent;
import au.thoughtworks.mypokemon.base.BasePresenter;
import au.thoughtworks.mypokemon.base.injection.PerActivity;
import au.thoughtworks.mypokemon.model.Pokemon;

import javax.inject.Inject;

import static au.thoughtworks.mypokemon.ui.details.DetailsActivity.EXTRA_POKEMON_DETAILS;

@PerActivity
public class DetailsPresenter extends BasePresenter<DetailsView> {

    @Inject
    public DetailsPresenter() {
    }

    public void onCreate(Intent intent) {
        Pokemon pokemon = (Pokemon) intent.getSerializableExtra(EXTRA_POKEMON_DETAILS);

        getView().showPokemonDetails(pokemon.getName(),
                pokemon.getAttack(),
                pokemon.getDefence(),
                pokemon.getType(),
                pokemon.getImage());
    }
}
