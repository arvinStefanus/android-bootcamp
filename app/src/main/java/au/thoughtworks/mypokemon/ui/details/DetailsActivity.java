package au.thoughtworks.mypokemon.ui.details;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import au.thoughtworks.mypokemon.R;
import au.thoughtworks.mypokemon.base.BaseActivity;
import au.thoughtworks.mypokemon.base.injection.ActivityComponent;
import com.squareup.picasso.Picasso;

public class DetailsActivity extends BaseActivity<DetailsPresenter> implements DetailsView {
    public static final String EXTRA_POKEMON_DETAILS = "ExtraPokemonDetails";

    private TextView nameTextView;
    private TextView attackTextView;
    private TextView defenceTextView;
    private TextView typeTextView;
    private ImageView pokemonImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter.attachView(this);

        nameTextView = (TextView) findViewById(R.id.activity_details_name_text);
        attackTextView = (TextView) findViewById(R.id.activity_details_attack_text);
        defenceTextView = (TextView) findViewById(R.id.activity_details_defence_text);
        typeTextView = (TextView) findViewById(R.id.activity_details_type_text);
        pokemonImage = (ImageView) findViewById(R.id.activity_details_image);

        presenter.onCreate(getIntent());
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showPokemonDetails(String name, int attack, int defence, String type, String image) {
        nameTextView.setText(name);
        attackTextView.setText(String.valueOf(attack));
        defenceTextView.setText(String.valueOf(defence));
        typeTextView.setText(type);

        Picasso.with(this).load(image).into(pokemonImage);
    }
}
