package au.thoughtworks.mypokemon.ui.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import au.thoughtworks.mypokemon.R;
import au.thoughtworks.mypokemon.model.Pokemon;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.activity_main_recycler_view);
        recyclerView.setAdapter(new PokemonListAdapter(
                Arrays.asList(
                        new Pokemon("pikachu", 5, 10, "electric", "http://img.pokemondb.net/artwork/mew.jpg"),
                        new Pokemon("pikachu", 5, 10, "electric", "http://img.pokemondb.net/artwork/mew.jpg"),
                        new Pokemon("pikachu", 5, 10, "electric", "http://img.pokemondb.net/artwork/mew.jpg"),
                        new Pokemon("pikachu", 5, 10, "electric", "http://img.pokemondb.net/artwork/mew.jpg"),
                        new Pokemon("pikachu", 5, 10, "electric", "http://img.pokemondb.net/artwork/mew.jpg"),
                        new Pokemon("pikachu", 5, 10, "electric", "http://img.pokemondb.net/artwork/mew.jpg"),
                        new Pokemon("pikachu", 5, 10, "electric", "http://img.pokemondb.net/artwork/mew.jpg"),
                        new Pokemon("pikachu", 5, 10, "electric", "http://img.pokemondb.net/artwork/mew.jpg"),
                        new Pokemon("pikachu", 5, 10, "electric", "http://img.pokemondb.net/artwork/mew.jpg"),
                        new Pokemon("pikachu", 5, 10, "electric", "http://img.pokemondb.net/artwork/mew.jpg"),
                        new Pokemon("pikachu", 5, 10, "electric", "http://img.pokemondb.net/artwork/mew.jpg")
                )));

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
