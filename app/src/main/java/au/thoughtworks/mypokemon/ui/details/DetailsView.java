package au.thoughtworks.mypokemon.ui.details;

import au.thoughtworks.mypokemon.base.BaseView;

public interface DetailsView extends BaseView {
    void showPokemonDetails(String name, int attack, int defence, String type, String image);
}
